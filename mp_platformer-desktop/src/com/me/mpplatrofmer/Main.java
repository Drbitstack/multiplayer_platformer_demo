package com.me.mpplatrofmer;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "mp_platformer";
		cfg.useGL20 = true;
		cfg.width = (int) (PlatformWithFriends.VIRTUAL_WIDTH * PlatformWithFriends.SCALE);
		cfg.height = (int) (PlatformWithFriends.VIRTUAL_HEIGHT * PlatformWithFriends.SCALE);
		
		new LwjglApplication(new PlatformWithFriends(), cfg);
	}
}
