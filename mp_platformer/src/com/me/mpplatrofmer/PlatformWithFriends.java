package com.me.mpplatrofmer;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool;
import com.me.mpplatrofmer.gamestates.GameStateManager;
import com.me.mpplatrofmer.gamestates.GameStateManager.GameState_ID;
import com.me.mpplatrofmer.handlers.InputHandler;


public class PlatformWithFriends implements ApplicationListener {
	
	public static final String TITLE = "GAMEY GAME";
	public static final float VIRTUAL_WIDTH = 160f;
	public static final float VIRTUAL_HEIGHT = 90f;
	
	public static final int SCALE = 8;//1280*720
	
	public static final float STEP = 1 / 60f;
	private float accum;
	
	private SpriteBatch sb;
	private OrthographicCamera cam;
	
	private GameStateManager gsm;
	private InputHandler inputHandler;

	private static final int MAX_RECTS = 500;
	
	private static final float RECT_SIZE = 5f;
	
	private Pool<Rectangle> rectanglePool;
	
	public static Content res;
	private FPSLogger flog;

	public void create() {
		
		inputHandler     = new InputHandler();
		Gdx.input.setInputProcessor(inputHandler);
		sb = new SpriteBatch();
		cam = new OrthographicCamera();
		cam.setToOrtho(false, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
		cam.update();
		Texture.setEnforcePotImages(false);
	
		allocateResources();
		
		//loadConfiguration()
		
		flog = new FPSLogger();
		gsm = new GameStateManager(this);
		
		gsm.setState(GameState_ID.MENU);
		
		allocateResources();
		
	}
	
	//make my game's rectangles?
	
	private void allocateResources() {
		
	 rectanglePool = new Pool<Rectangle>(MAX_RECTS) {
			@Override
			protected Rectangle newObject () {
				return new Rectangle();
			}
		};
		
		res = new Content();
		//res.loadTexture("res/images/menu.png");

//		res.loadSound("res/sfx/jump.wav");
//		
//		res.loadMusic("res/music/bbsong.ogg");
//		res.getMusic("bbsong").setLooping(true);
//		res.getMusic("bbsong").setVolume(0.5f);
//		res.getMusic("bbsong").play();
		
	}
	
	public  Pool<Rectangle> getRectanglePool()
	{
		return rectanglePool;
	}

	public void render() {
		
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		float step = Gdx.graphics.getDeltaTime();
		
		if(step > .1f)
		{
			step = .1f;
		}
		
		flog.log();
		
		gsm.render();
		gsm.update(step);

//		
//		accum += Gdx.graphics.getDeltaTime();
//		while(accum >= STEP) {
//			//TODO do I need these?
//
//			//--
//			accum -= STEP;
//			gsm.update(STEP);
//			gsm.render();
//		}
		
	}
	
	public void dispose() {
		
	}
	
	public SpriteBatch getSpriteBatch() 
	{ 
		return sb; 
	}
	public OrthographicCamera getCamera() 
	{ 
		return cam; 
	}
	public InputHandler getInputHandler() 
	{
		return inputHandler;
	}
	
	public void resize(int w, int h) 
	{
		gsm.resize(w,h);
	}
	public void pause() {}
	public void resume() {}

}
