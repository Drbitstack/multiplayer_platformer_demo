package com.me.mpplatrofmer.Util;

import com.badlogic.gdx.math.Rectangle;

public class Utilities {

	//UTILITY FUNCTION
			public static boolean getOverlap(Rectangle rect1, Rectangle rect2) {

				double r1x1, r1x2, r1y1, r1y2, r2x1, r2x2, r2y1, r2y2; 
				
				r1x1 = rect1.x;
				r1x2 = rect1.x+rect1.width;
				r1y1 = rect1.y;
				r1y2 = rect1.y+rect1.height;
				r2x1 = rect2.x;
				r2x2 = rect2.x+rect2.width;
				r2y1 = rect2.y;
				r2y2 = rect2.y+rect2.height;
				
				double xval = Math.max(0, (Math.min(r2x2, r1x2) - Math.max(r1x1, r2x1)));
				double yval = Math.max(0, (Math.min(r2y2, r1y2) - Math.max(r1y1, r2y1)));
				
				if( xval > 0 && yval > 0 )
				{
					//make the rectangle
					return true;
					//return new Rectangle((float)Math.max(r1x1, r2x1), (float)Math.max(r1y1, r2y1), (float)xval, (float)yval);
				}
				return false;
				//return null;
			}
			
			

}
