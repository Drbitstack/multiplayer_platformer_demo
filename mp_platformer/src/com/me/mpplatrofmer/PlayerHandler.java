package com.me.mpplatrofmer;

import java.util.ArrayList;
import java.util.List;



public class PlayerHandler implements PlayerListener{
	
	private ArrayList<PlayerListener> handlers;
	
	public PlayerHandler()
	{
		super();
		handlers = new ArrayList<PlayerListener>();
	}
	
	public List<PlayerListener> getHandlers() {
		return handlers;
	}
	
	public void addListener(PlayerListener handler) {
		this.handlers.add(handler);
	}

	public void removeListener(PlayerListener listener) {
		this.handlers.remove(listener);
	}

	@Override
	public void isMovingLeft(float dt) {
		for(int i=0;i<handlers.size();++i)
		{
			handlers.get(i).isMovingLeft(dt);
		}
	}

	@Override
	public void isMovingRight(float dt) {
		for(int i=0;i<handlers.size();++i)
		{
			handlers.get(i).isMovingRight(dt);
		}	
	}

	@Override
	public void isNotMoving(float dt) {
		for(int i=0;i<handlers.size();++i)
		{
			handlers.get(i).isNotMoving(dt);
		}
		
	}

}
