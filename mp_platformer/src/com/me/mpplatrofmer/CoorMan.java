package com.me.mpplatrofmer;

import com.badlogic.gdx.Gdx;

public class CoorMan {
	
	public static final int PIXELS_PER_METER = 180;
	
//	public static float PIXELS_TO_WORLD = PlatformWithFriends.VIRTUAL_WIDTH/Gdx.app.getGraphics().getWidth();
//	public static float WORLD_TO_PIXELS = 1/PIXELS_TO_WORLD;
//	
//	public static float WORLD_TO_BOX = WORLD_TO_PIXELS/PIXELS_PER_METER;
//	public static float BOX_TO_WORLD = 1/WORLD_TO_BOX;
//	
//	public static float BOX_TO_PIXELS = BOX_TO_WORLD*WORLD_TO_PIXELS;
//	public static float PIXELS_TO_BOX = PIXELS_TO_WORLD*WORLD_TO_BOX;
	
	public static float worldToBox(float world) {
		
		return world*calcWorldToBox();
	}
	
	public static float boxToWorld(float box) {
		return box*calcBoxToWorld();
	}

	public static float worldToPixels(float world) {
		return world*calcWorldToPixels();
	}
	
	public static float boxToPixels(float box) {
		return box*calcBoxToPixels();
	}
	
	public static float pixelsToBox(float pixels)
	{
		return pixels*calcPixelsToBox();
	}
	
	public static float pixelsToWorld(float pixels)
	{
		return pixels*calcPixelsToWorld();
	}
	
	
	//helper, everything is generated from real-time data
	//TODO maybe this should instead be reliant on the camera viewport instead of the constant virtual value
	private static float calcPixelsToWorld() {
		return PlatformWithFriends.VIRTUAL_WIDTH/(PlatformWithFriends.VIRTUAL_WIDTH*PlatformWithFriends.SCALE);
	}
	
	private static float calcWorldToPixels() {
		return 1/calcPixelsToWorld();
	}
	
	private static float calcWorldToBox() 
	{
		return calcWorldToPixels()/PIXELS_PER_METER;
	}

	private static float calcBoxToWorld() 
	{
		return 1/calcWorldToBox();
	}
	
	private static float calcBoxToPixels() {
		return calcBoxToWorld()*calcWorldToPixels();
	}
	
	private static float calcPixelsToBox()
	{
		return calcPixelsToWorld()*calcWorldToBox();
	}


}
