package com.me.mpplatrofmer.gamestates;

import java.util.List;

import com.badlogic.gdx.InputAdapter;
import com.me.mpplatrofmer.handlers.KeyListener;
import com.me.mpplatrofmer.handlers.ManagedKey;

public class ManagedInputAdapter extends InputAdapter {
	
	private List<ManagedKey> managedKeys;
	private KeyListener keyListener;
	private boolean isMuted = false;
	
	public ManagedInputAdapter( List<ManagedKey> managedKeys, KeyListener listener )
	{
		this.managedKeys = managedKeys;
		this.keyListener = listener;
	}

	public void setMuted(boolean val)
	{
		isMuted = val;
	}
	
	public boolean isMuted()
	{
		return isMuted;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		for(int i=0;i<managedKeys.size();++i)
		{
			if(managedKeys.get(i).getMyCode() == keycode)
			{
				managedKeys.get(i).setDown();
				if(!isMuted()) keyListener.doDownAction(managedKeys.get(i));
			}
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		for(int i=0;i<managedKeys.size();++i)
		{
			if(managedKeys.get(i).getMyCode() == keycode)
			{
				managedKeys.get(i).setUp();
				if(!isMuted()) keyListener.doUpAction(managedKeys.get(i));
			}
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == character)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == keycode)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return true;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == keycode)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == keycode)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return true;
	}

	@Override
	public boolean mouseMoved(int x, int y) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == keycode)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return true;
	}

	@Override
	public boolean scrolled(int amount) {
//		for(int i=0;i<managedKeys.size();++i)
//		{
//			if(managedKeys.get(i).getMyCode() == keycode)
//			{
//				managedKeys.get(i).setDown(true);
//			}
//		}
		return true;
	}
	

}
