package com.me.mpplatrofmer.gamestates;

import com.me.mpplatrofmer.gamestates.GameStateManager.GameState_ID;

public class PauseState extends GameState{

	protected PauseState(GameStateManager gsm) {
		super(gsm);
		this.gs_ID = GameState_ID.PAUSE;
	}

	@Override
	public void handleInput()    {
		
	}

	@Override
	public void update(float dt) {
		
	}

	@Override
	public void render() {
	
	}

	@Override
	public void dispose() {

		this.dispose();
	}

	@Override
	public void pause() {
		
		
	}

	@Override
	public void resize(int w, int h) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

}
