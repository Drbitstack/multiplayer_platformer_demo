package com.me.mpplatrofmer.gamestates;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.me.mpplatrofmer.Player;
import com.me.mpplatrofmer.PlayerHandler;
import com.me.mpplatrofmer.gamestates.PlayState.key_index;
import com.me.mpplatrofmer.handlers.KeyListener;
import com.me.mpplatrofmer.handlers.ManagedKey;

public class PlayerController implements KeyListener {
	
	public static final float WALK_IMPULSE = 3f;
	private static final int  JUMPS_ALLOWED = 1;
	private static final long  JUMP_TIME_MIN = 100; //ms
	private static final float JUMP_TIME_MAX = .1f;
	private static final float JUMP_FORCE = 25f;

	
	private int jumpCounter, jumpAccumulator;
	private long jumpPressedTime;
	private boolean jumpingPressed;
	
	
	private ManagedInputAdapter playerKeyListener;
	private PlayerHandler playerStateHandler;


	private List<ManagedKey> keyList;
	private Player player;

	public PlayerController(Player player) {

		this.player = player; 
		
		playerStateHandler = player.getPlayerHandler();
		
		initialize();
		
		configureKeyListener();	
		
	}

	private void initialize() {
		jumpCounter = 0;
		jumpAccumulator = 0;
		jumpPressedTime = 0;
		jumpingPressed = false;		
	}

	private void configureKeyListener() {
		
		keyList = new ArrayList<ManagedKey>(){{
			add( new ManagedKey(Keys.A));
			add( new ManagedKey(Keys.D));
			add( new ManagedKey(Keys.SPACE));
		}};
		
		playerKeyListener = new ManagedInputAdapter(keyList, this);
		
	}
	
	public void update(float delta)
	{
		
		Vector2 velocity = new Vector2(player.getBody().getLinearVelocity());
		Vector2 pos = new Vector2(player.getBody().getPosition());
		
		Vector2 move = player.getBody().getLinearVelocity();
		Vector2 move2 = player.getBody().getLinearVelocity();

		float velocityX = velocity.x;
		float velocityY = velocity.y;
		
		/* CONTROL LEFT-RIGHT MOTION */
		if( keyList.get(key_index.A.getIndex()).isDown() 
		&&  keyList.get(key_index.D.getIndex()).isUp()  )
		{
			playerStateHandler.isMovingLeft(delta);
			player.setFacingLeft(true);
			move.x = -WALK_IMPULSE;
//			player.getBody().setLinearVelocity(move);	
			player.getBody().applyLinearImpulse(move, pos, true);
			
			if( velocityX > 0 )
			{
				//velocityX *= DIRECTION_CHANGE_DAMPENER;
				//if(Math.abs(velocityX) < 3f) velocityX = 0;
			}
		}
		else if( keyList.get(key_index.A.getIndex()).isUp() 
			 &&  keyList.get(key_index.D.getIndex()).isDown()  )
		{
			playerStateHandler.isMovingRight(delta);
			player.setFacingLeft(false);
			move.x = WALK_IMPULSE;
 			player.getBody().applyLinearImpulse(move, pos, true);
			if( velocityX < 0 )
			{
				//velocityX *= DIRECTION_CHANGE_DAMPENER;
				//if(Math.abs(velocityX) < 3f) velocityX = 0;
			}
		}
		else //either both or none
		{
			playerStateHandler.isNotMoving(delta);
			//accelerationX = 0f;
			//velocityX *= STOP_DAMPENER;
			//if(Math.abs(velocityX) < 3f) velocityX = 0;
		}
		
		/* CONTROL UP-DOWN MOTION */
		


		if( keyList.get(key_index.SPACE.getIndex()).isDown() ) //is jumping until grounded...
		{
			
			if( jumpCounter <= JUMPS_ALLOWED )
			{
	
				if(jumpAccumulator < JUMP_TIME_MAX )
				{
					move2.y = JUMP_FORCE;
					player.getBody().applyLinearImpulse(move2, pos, true);
 					//player.getBody().applyForceToCenter(new Vector2(0,JUMP_FORCE), true);
					jumpingPressed = true;
					jumpCounter++;
				}
				jumpAccumulator += delta;
			}
		}
		else 
		{
			if (jumpingPressed && ((System.currentTimeMillis() - jumpPressedTime) >= JUMP_TIME_MIN)) 
			{
				jumpingPressed = false;
			} 
			else {
				if (jumpingPressed) 
				{
					//player.getBody().applyForceToCenter(new Vector2(0,JUMP_FORCE), true);
					jumpAccumulator += delta;
				}
			}
		}
	}

	public void setJumpCounter(int jumpCounter) {
		this.jumpCounter = jumpCounter;
	}

	@Override
	public void doDownAction(ManagedKey managedKey) {
		if(managedKey.getMyCode() == Keys.A)
		{
			
		}
		else if(managedKey.getMyCode() == Keys.D)
		{
			
		}
		else if(managedKey.getMyCode() == Keys.SPACE)
		{
			if( jumpCounter < JUMPS_ALLOWED )
			{
				jumpPressedTime = managedKey.getLastDownTime();
				jumpAccumulator = 0;
			}
		}
		else if(managedKey.getMyCode() == Keys.ESCAPE)
		{
			
		}	
	}

	@Override
	public void doUpAction(ManagedKey managedKey) {
		if(managedKey.getMyCode() == Keys.A)
		{
			
		}
		else if(managedKey.getMyCode() == Keys.D)
		{
			
		}
		else if(managedKey.getMyCode() == Keys.SPACE)
		{

		}
		else if(managedKey.getMyCode() == Keys.ESCAPE)
		{
			
		}
		
	}
	
	public PlayerHandler getPlayerStateHandler() {
		return playerStateHandler;
	}

	public ManagedInputAdapter getPlayerKeyListener() {
		return playerKeyListener;
	}


}
