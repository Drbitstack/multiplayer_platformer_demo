package com.me.mpplatrofmer.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.me.mpplatrofmer.gamestates.GameStateManager.GameState_ID;

public class MenuState extends GameState {
	
	private Stage stage;

	private Skin skin;
	private Table table;

	private Button button_newServer, button_findServer, button_play, button_settings; 
	private List serverList;
	
	private int width, height;

	protected MenuState(GameStateManager gsm) {
		super(gsm);
		this.gs_ID = GameState_ID.MENU;
		
		makeStage();
		
		setHandlers();
		
	}

	private void makeStage() {
		sb.flush();
		stage = new Stage(width, height, false, sb);
		table = new Table();
		
		skin = new Skin();
		
		stage.addActor(table);

		Pixmap pixmap = new Pixmap(1, 1, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));

		// Store the default libgdx font under the name "default".
		skin.add("default", new BitmapFont());

		// Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("white", Color.WHITE);
		textButtonStyle.down = skin.newDrawable("white", Color.BLUE);
		textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
		textButtonStyle.font = skin.getFont("default");
		textButtonStyle.fontColor = Color.DARK_GRAY;
		skin.add("default", textButtonStyle);
		
		
		ListStyle listStyle = new ListStyle();
		listStyle.font = skin.getFont("default");
		listStyle.fontColorSelected = Color.CYAN;
		listStyle.fontColorUnselected = Color.WHITE;
		
		button_newServer    = new TextButton("New Server", skin);
		button_findServer   = new TextButton("Find Server", skin);
		button_play 		= new TextButton("1P Mode", skin);
		button_settings 	= new TextButton("Settings", skin);
		//serverList 			= new List(listStyle);
		
		table.add(button_newServer).pad(10);	
		table.row();
		table.add(button_findServer).pad(10);
		table.row();
		table.add(button_play).pad(10);
		table.row();
		table.add(button_settings).pad(10);

		table.setFillParent(true);
		
		button_newServer.pad(20);
		button_findServer.pad(20);
		button_play.pad(20);
		button_settings.pad(20);
		
		table.pad(64);
		table.pack();
		
		button_play.addListener(new ChangeListener()
		{

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				
				//button_play.addAction(Actions.moveTo(10f,10f, 10f));
				
				gsm.setState(GameState_ID.PLAY);
			}
			
		});
		width = Gdx.app.getGraphics().getWidth();
		height = Gdx.app.getGraphics().getHeight();
		stage.setViewport(width, height, true);
	}


	@Override
	public void handleInput() {
		
	}

	@Override
	public void update(float step) {
		
		stage.act(step);
	}

	@Override
	public void render() {
		
		stage.draw();
		Table.drawDebug(stage);
		
	}
	
	@Override
	public void pause() {
		
		//unregister my listener
		//change local step size to 0!
		
	}
	
	public void resize(int w, int h)
	{
		this.width  = w;
		this.height = h;
		stage.setViewport(width, height, true);

	}
	
	private void setHandlers() {
		
		game.getInputHandler().addListener(stage);
		
	}

	@Override
	public void dispose() 
	{
		game.getInputHandler().removeListener(stage);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}



}
