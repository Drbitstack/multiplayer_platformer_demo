package com.me.mpplatrofmer.gamestates;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.me.mpplatrofmer.AnimatedB2DSprite;
import com.me.mpplatrofmer.CoorMan;
import com.me.mpplatrofmer.Level;
import com.me.mpplatrofmer.PlatformWithFriends;
import com.me.mpplatrofmer.Player;
import com.me.mpplatrofmer.gamestates.GameStateManager.GameState_ID;

public class PlayState extends GameState {
	
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private Level level;
	private Player player;
	
	AnimatedB2DSprite p1;
	TextureRegion tf;
	AnimatedB2DSprite p2;
	TextureRegion tf2;
	 Body bottleModel;
	 Body bottleModel2;

	private OrthographicCamera 	b2dCam;
	
	private Matrix4 debugMatrix;
	
	private RayHandler rh;
	

	//private GameClient gc;
	
	public RayHandler getRh() {
		return rh;
	}

	public OrthographicCamera getB2dCam() {
		return b2dCam;
	}

	public static enum key_index
	{
		A(00),
		D(01),
		SPACE(02),
		ESCAPE(03);
		
		private int key_index;
		
		key_index(int index)
		{
			this.key_index = index;
		}
		
		public int getIndex()
		{
			return this.key_index;
		}
		
	};
	

	protected PlayState(GameStateManager gsm) {
		super(gsm);
		this.gs_ID = GameState_ID.PLAY;
		
		makeWorld();
		
		makePlayer();
		
		makeLevel();
		
		setHandlers();
		
		
		//createBottle();
		//createBottle2();

	}

	private void makeWorld() {
		
		world = new World(new Vector2(0, -10), true); 
		
		rh = new RayHandler(world);
		
	}

	private void makePlayer() {

		BodyDef bodyDef2 = new BodyDef();
		bodyDef2.type = BodyType.DynamicBody;
		bodyDef2.position.set(CoorMan.worldToBox(20f), CoorMan.worldToBox(30f));
		Body body2 = world.createBody(bodyDef2);
		
		PolygonShape p = new PolygonShape();
		p.setAsBox(CoorMan.worldToBox(Player.PLAYER_WIDTH/2), CoorMan.worldToBox(Player.PLAYER_HEIGHT/2));

		FixtureDef fixtureDef2= new FixtureDef();
		fixtureDef2.shape = p;
		fixtureDef2.density = 80f; 
		fixtureDef2.friction = 0.4f;
		fixtureDef2.restitution = .01f; 
		Fixture fixture2 = body2.createFixture(fixtureDef2);
		body2.setFixedRotation(true);
		
		player = new Player(body2, game);
		body2.setUserData("player1");
	
		//make the player's feet
		p.setAsBox(CoorMan.worldToBox(Player.PLAYER_WIDTH/2), CoorMan.worldToBox(.5f), 
				new Vector2(0f, -CoorMan.worldToBox(Player.PLAYER_HEIGHT/2)+CoorMan.worldToBox(.5f)), 0f) ;
		fixtureDef2.isSensor = true;
		body2.createFixture(fixtureDef2);
		p.dispose();

		
	}
	
	private void createBottle2() {
		// TODO Auto-generated method stub
	    // 0. Create a loader for the file saved from the editor.
	    BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/test.json"));
	    
	    // 1. Create a BodyDef, as usual.
	    BodyDef bd = new BodyDef();
	    bd.position.set(CoorMan.worldToBox(80f), CoorMan.worldToBox(45f));
	    bd.type = BodyType.DynamicBody;

	 
	    // 2. Create a FixtureDef, as usual.
	    FixtureDef fd = new FixtureDef();
	    fd.density = 10f;
	    fd.friction = 0.5f;
	    fd.restitution = 0.2f;
	 
	    // 3. Create a Body, as usual.
	    bottleModel2 = world.createBody(bd);
	    bottleModel2.setBullet(true);
	    int scale = 4;
	    // 4. Create the body fixture automatically by using the loader.
	    loader.attachFixture(bottleModel2, "p2", fd, scale);
	    Texture t = new Texture(Gdx.files.internal("data/testtest.png"));
	    tf2 = new TextureRegion(t);
	    
	    //p2 = new AnimatedB2DSprite(bottleModel2, new Vector2(12f*scale, 36f*scale));
	}

	private void createBottle() {
	    // 0. Create a loader for the file saved from the editor.
	    BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/test.json"));
	    
	    // 1. Create a BodyDef, as usual.
	    BodyDef bd = new BodyDef();
	    bd.position.set(CoorMan.worldToBox(80f), CoorMan.worldToBox(45f));
	    bd.type = BodyType.DynamicBody;

	 
	    // 2. Create a FixtureDef, as usual.
	    FixtureDef fd = new FixtureDef();
	    fd.density = 10f;
	    fd.friction = 0.5f;
	    fd.restitution = 0.2f;
	 
	    // 3. Create a Body, as usual.
	    bottleModel = world.createBody(bd);
	    bottleModel.setBullet(true);
	    // 4. Create the body fixture automatically by using the loader.
	    loader.attachFixture(bottleModel, "test01", fd, (float) 0.5);
	    Texture t = new Texture(Gdx.files.internal("data/test01.png"));
	    tf = new TextureRegion(t);
	    p1 = new AnimatedB2DSprite(bottleModel, new Vector2(12f, 12f));
	}

	private void setHandlers() {
		
		world.setContactListener(player.getContactListener());
		
	}

	private void makeLevel() {
		

		debugRenderer = new Box2DDebugRenderer();
		b2dCam = new OrthographicCamera(CoorMan.worldToBox(PlatformWithFriends.VIRTUAL_WIDTH), 
				CoorMan.worldToBox(PlatformWithFriends.VIRTUAL_HEIGHT));
		
		debugMatrix = new Matrix4(b2dCam.combined);
		
	    debugMatrix.translate(-b2dCam.viewportWidth/2, -b2dCam.viewportHeight/2, 0);
	    
		level = new Level(this, game);
	    
	      //scale the debug matrix by the scaling so everything looks normal
	    //debugMatrix.scale(CoorMan.BOX_TO_WORLD, CoorMan.BOX_TO_WORLD, 0);
	}
	
	
	@Override
	public void pause() {
		// TODO 
		setPaused(true);

	}
	

	@Override
	public void resume() {
		setPaused(false);

	}

	private void setPaused(boolean val) {
		this.isPaused = val;		
	}

	@Override
	public void handleInput() {
		
	}

	@Override
	public void update(float delta) {
		
		player.update(delta);
		
		rh.update();
		world.step(1/60f, 6, 2);
	}

	@Override
	public void render() {
		
		cam.update();
		
		sb.setProjectionMatrix(cam.combined);
		 
		//debugRenderer.render(world, debugMatrix);

		b2dCam.update();
		player.render(sb);
		level.render();
		//rh.render();
		//pRender();
		
		//render networked items here

	}

	private void pRender() {

		p1.render(sb, tf);
		//p2.render(sb, tf2);
	}

	 
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}

	@Override
	public void dispose() 
	{
		
	}

	@Override
	public void resize(int w, int h) {
		
	}



}
