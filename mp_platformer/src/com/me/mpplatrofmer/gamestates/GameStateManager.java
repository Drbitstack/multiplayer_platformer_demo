package com.me.mpplatrofmer.gamestates;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.mpplatrofmer.PlatformWithFriends;
import com.me.mpplatrofmer.handlers.KeyListener;
import com.me.mpplatrofmer.handlers.ManagedKey;

public class GameStateManager implements KeyListener {
	
	public static enum GameState_ID
	{
		PLAY(00), PAUSE(01), MENU(02), INVALID(03);	
		
		private int gs_ID;
		private GameState_ID(int gs_ID)
		{
			this.gs_ID = gs_ID;
		}
		public int getID()
		{
			return gs_ID;
		}
	}
	
	private PlatformWithFriends game;
	private OrthographicCamera cam;
	private SpriteBatch sb;
	
	private GameState activeGameState;
	
	private GameState states[];
	
	private InputAdapter stateListener;
	private List<ManagedKey> stateKeyList;

	public GameStateManager(PlatformWithFriends game) {
		this.game = game;
		this.cam = game.getCamera();
		this.sb = game.getSpriteBatch();
		
		makeStates();
		
		stateKeyList = new ArrayList<ManagedKey>(){{
			add( new ManagedKey(Keys.ESCAPE));
		}};
		
		stateListener	   = new ManagedInputAdapter(stateKeyList, this);
		game.getInputHandler().addListener(stateListener);
		
	}

	private void makeStates() {
		
		states = new GameState[3];
		
//		for(GameState_ID id : GameState_ID.values())
//		{
//			states[id.gs_ID] = makeGameState(id);
//		}
		
	}

	
	public void setState( GameState_ID gs_ID )
	{
		if( states[gs_ID.gs_ID] == null )
		{
			states[gs_ID.gs_ID] = makeGameState(gs_ID);
			if( getActiveState() == null ) setActiveState(states[gs_ID.gs_ID]); //first time through
		}
		//if we successfully made the new state or it already exists, do transition
		if( states[gs_ID.gs_ID] != null )
		{
			transitionStates( getActiveState().gs_ID , gs_ID );
		} 
		//else, don't do anything
	}

	private void setActiveState(GameState gameState) {
		activeGameState = gameState;
		if(activeGameState.isPaused) activeGameState.resume();
	}

	private void transitionStates(GameState_ID from, GameState_ID to ) {
		//from play to pause
		
		GameState lastState = getActiveState();
		
		if( to != from )
		{
			if( from == GameState_ID.PLAY 
			&&  to   == GameState_ID.PAUSE  )
			{
				//animations?
				
				setActiveState(states[to.gs_ID]);
				
			}
			else if( from == GameState_ID.PAUSE 
				 &&  to   == GameState_ID.PLAY  )
			{
				
				setActiveState(states[to.gs_ID]);
				lastState.dispose();
				states[from.getID()] = null; //remove reference
			}
			else if( from == GameState_ID.PLAY 
				 &&  to   == GameState_ID.MENU  )
			{
				
				
				setActiveState(states[to.gs_ID]);
			}
			else if( from == GameState_ID.MENU 
				 &&  to   == GameState_ID.PLAY  )
			{
				
				setActiveState(states[to.gs_ID]);
				lastState.dispose();
				states[from.getID()] = null; //remove reference
			}
		}
		else
		{
			//aleady in the right state
		}
	
	}

	public GameState makeGameState( GameState_ID gs_ID )
	{
		switch(gs_ID)
		{
			case PLAY:
				return new PlayState(this);
			case PAUSE:
				return new PauseState(this);
			case MENU:
				return new MenuState(this);
			case INVALID:
			default:
				return null;
		}
	}

	private GameState_ID lookupGameState(int gs_ID) {	
        for (GameState_ID id : GameState_ID.values()) {
            if (id.getID() == gs_ID) {
                return id;
            }
        }
        return GameState_ID.INVALID;
	}
	
	public void update(float step) 
	{
		activeGameState.update(step);
	}

	public void render() {
		activeGameState.render();
	}
	
	public void resize(int w, int h)
	{
		//who needs to know?
		activeGameState.resize(w, h);
	}
	

	public PlatformWithFriends game() {
		return game;
	}
	
	public GameState getActiveState()
	{
		return activeGameState;
	}

	@Override
	public void doDownAction(ManagedKey managedKey) {
		
		switch(managedKey.getMyCode())
		{
			case Keys.ESCAPE:
							/* For the escape key */
							switch(getActiveState().gs_ID)
							{
							case PLAY:
									getActiveState().pause();
									setState(GameState_ID.MENU);
								break;
							case MENU:
								break;
							case PAUSE:
								break;
							default:
								break;
							}
							break;
			default:
				break;
		}
		
	}

	@Override
	public void doUpAction(ManagedKey managedKey) {
		// TODO Auto-generated method stub
		
	}
	

}
