package com.me.mpplatrofmer.gamestates;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.me.mpplatrofmer.Player;

public class PlayerContactListener implements ContactListener {

	private Player player;
	
	public PlayerContactListener( Player player )
	{
		this.player = player;
	}
	
	@Override
	public void beginContact(Contact contact) {
		
		Body A = contact.getFixtureA().getBody();
		Body B = contact.getFixtureB().getBody();
		
		if(  A.getUserData() == "player1" &&
			 B.getUserData() == "floor")
		{
			System.out.println("TOUCHINGLOL");
			player.getPlayerController().setJumpCounter(0); //reset jumps
		}

		return;
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
