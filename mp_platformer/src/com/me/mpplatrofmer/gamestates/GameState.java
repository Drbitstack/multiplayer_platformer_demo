package com.me.mpplatrofmer.gamestates;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.mpplatrofmer.PlatformWithFriends;
import com.me.mpplatrofmer.gamestates.GameStateManager.GameState_ID;

public abstract class GameState {
	
	
	protected GameStateManager gsm;
	protected PlatformWithFriends game;
	
	protected SpriteBatch sb;
	protected OrthographicCamera cam;
	
	protected GameState_ID gs_ID;
	
	protected boolean 	isPaused;
	
	protected GameState(GameStateManager gsm) {
		this.gsm = gsm;
		game = gsm.game();
		sb = game.getSpriteBatch();
		cam = game.getCamera();
	}
	
	public abstract void handleInput();
	public abstract void update(float dt);
	public abstract void render();
	public abstract void dispose();
	public abstract void pause();
	public abstract void resume();
	public abstract void resize(int w, int h);
	
}
