package com.me.mpplatrofmer;

public interface PlayerListener {
	
	void isMovingLeft(float dt);
	void isMovingRight(float dt);
	void isNotMoving(float dt);
	

}
