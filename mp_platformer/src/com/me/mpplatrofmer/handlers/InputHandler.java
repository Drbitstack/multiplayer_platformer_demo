package com.me.mpplatrofmer.handlers;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor {
	
	private List<InputAdapter> listeners;
	
	public InputHandler()
	{
		super();
		listeners = new ArrayList<InputAdapter>();
	}
	
	public List<InputAdapter> getListeners() {
		return listeners;
	}

	public void setListeners(List<InputAdapter> listeners) {
		this.listeners = listeners;
	}
	
	public void addListener(InputAdapter listener) {
		this.listeners.add(listener);
	}

	public void removeListener(InputAdapter listener) {
		this.listeners.remove(listener);
	}

	@Override
	public boolean keyDown(int keycode) {
		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).keyDown(keycode);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).keyUp(keycode);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).keyTyped(character);
		}
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
			for(int i=0;i<listeners.size();++i)
			{
				listeners.get(i).touchDown(x, y, pointer, button);
			}
		return true;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
			for(int i=0;i<listeners.size();++i)
			{
				listeners.get(i).touchUp(x, y, pointer, button);
			}
		return true;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).touchDragged(x, y, pointer);
		}
		return true;
	}

	@Override
	public boolean mouseMoved(int x, int y) {
		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).mouseMoved(x, y);
		}
		return true;
	}

	@Override
	public boolean scrolled(int amount) {

		for(int i=0;i<listeners.size();++i)
		{
			listeners.get(i).scrolled(amount);
		}
		return true;
	}


}
