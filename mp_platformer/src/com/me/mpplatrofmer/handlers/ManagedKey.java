package com.me.mpplatrofmer.handlers;


public class ManagedKey {
	
	private int myCode; //who am i
	private boolean isDown;
	private boolean isUp;
	private long downCount;
	private long upCount;
	private long lastDownTime;
	private long lastUpTime;
	
	//keyHistoriser?

	public ManagedKey(int keyCode)
	{
		initialize();
		setMyCode(keyCode);
	}
	
	
	public void resetDownCount()
	{
		this.downCount = 0;
	}
	
	
	public void resetUpCount()
	{
		this.upCount = 0;
	}
	
	
	public void resetCounts()
	{
		resetDownCount();
		resetUpCount();
	}

	private void initialize()
	{
		this.isDown  	  = false;
		this.isUp      	  = true;
		this.downCount	  = 0;
		this.upCount  	  = 0;
		this.lastDownTime = 0;
		this.lastUpTime   = 0;
	}
	
	/* Getters and Setters */
	
	public boolean isDown() {
		return isDown;
	}

	public ManagedKey setDown() {
		this.isUp = false;
		this.isDown = true;
		this.downCount++;
		this.lastDownTime = System.currentTimeMillis();
		return this;
	}

	public boolean isUp() {
		return isUp;
	}

	public ManagedKey setUp() {
		this.isDown = false;
		this.isUp = true;
		this.upCount++;
		this.lastUpTime = System.currentTimeMillis();
		return this;
	}

	public long getDownCount() {
		return downCount;
	}

	public ManagedKey setDownCount(long downCount) {
		this.downCount = downCount;
		return this;
	}

	public long getUpCount() {
		return upCount;
	}

	public ManagedKey setUpCount(long upCount) {
		this.upCount = upCount;
		return this;
	}

	public long getLastDownTime() {
		return lastDownTime;
	}

	public ManagedKey setLastDownTime(long lastDownTime) {
		this.lastDownTime = lastDownTime;
		return this;
	}

	public long getLastUpTime() {
		return lastUpTime;
	}

	public ManagedKey setLastUpTime(long lastUpTime) {
		this.lastUpTime = lastUpTime;
		return this;
	}


	public int getMyCode() {
		return myCode;
	}


	public ManagedKey setMyCode(int myCode) {
		this.myCode = myCode;
		return this;
	}


}
