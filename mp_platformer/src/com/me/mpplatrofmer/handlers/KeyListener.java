package com.me.mpplatrofmer.handlers;

public interface KeyListener {
	
	//EDGE ACTIONS HERE
	public void doDownAction(ManagedKey managedKey);

	public void doUpAction(ManagedKey managedKey);
}
