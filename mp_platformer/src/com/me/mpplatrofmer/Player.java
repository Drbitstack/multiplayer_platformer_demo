package com.me.mpplatrofmer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.me.mpplatrofmer.gamestates.PlayerContactListener;
import com.me.mpplatrofmer.gamestates.PlayerController;

public class Player extends AnimatedB2DSprite{

	private String name;
	private PlatformWithFriends game;
	private boolean isFacingLeft;
	private static PlayerAnimationHandler playerAnimationHandler;

	private PlayerController playerController;


	private PlayerHandler playerStateHandler;
	private PlayerContactListener playerContactListener;
	
	//base player size
	public static final float PLAYER_WIDTH  = 10f;
	public static final float PLAYER_HEIGHT = 10f;
	
	public enum PLAYER_STATE {
		WALKING(00),
		AIRBOURNE(01),
		IDLE(02),
		INVALID(03);

		private int state;
		private PLAYER_STATE(int state)
		{
			this.state = state;
		}
		public int getState() {
			return state;
		}
	}

	public Player(Body body, PlatformWithFriends game) {
		
		super(body, new Vector2(PLAYER_WIDTH, PLAYER_HEIGHT));	
		
		this.game = game; 
		
		setHandlers();
		
		//create and set all animations for this player
		configureAnimations();
		
	}
	
	private void setHandlers()
	{
		//do player controller
		
		playerStateHandler = new PlayerHandler(); //MUST COME BEFORE CONTROLLER, shitty code shit shit shit I suck
		
		playerController = new PlayerController(this);
		
		game.getInputHandler().addListener(playerController.getPlayerKeyListener());
		
		//do player contact listener
		playerContactListener = new PlayerContactListener(this);
		

	}
	
	@Override
	public void update(float delta)
	{
		playerController.update(delta);
		animation.update(delta);
	}

	private void configureAnimations() {
		
		//Create the animation handler for the player. It must be given
		//each animation that the player is capable of, and it takes care of 
		//event-based animation switching
		playerAnimationHandler = new PlayerAnimationHandler(this); 	
		
		TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("sprites/Textures/textures.pack"));
		
		TextureRegion[] walkLeftFrames = new TextureRegion[4];
		TextureRegion[] walkRightFrames = new TextureRegion[4];
		
		for(int i = 0; i < 4; i++) {
			walkLeftFrames[i] = new TextureRegion(atlas.findRegion("bob-0" + (i + 1)));
			walkRightFrames[i] = new TextureRegion(atlas.findRegion("bob-0" + (i + 1)));
			walkRightFrames[i].flip(true, false);
		}

		playerAnimationHandler.addAnimation(walkLeftFrames, 1/12f);
		playerAnimationHandler.addAnimation(walkRightFrames, 1/12f);
		
		playerAnimationHandler.setInitialState();
		
		width = walkLeftFrames[0].getRegionWidth();
		height = walkLeftFrames[0].getRegionHeight();
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isFacingLeft() {
		return isFacingLeft;
	}

	public void setFacingLeft(boolean isFacingLeft) {
		this.isFacingLeft = isFacingLeft;
	}

	public static PLAYER_STATE lookupState(String string) {

		switch(string)
		{

		case "WALKING":
			return PLAYER_STATE.WALKING;
		case "AIRBOURNE":
			return PLAYER_STATE.AIRBOURNE;
		case "IDLE":
			return PLAYER_STATE.INVALID;
		case "INVALID":
		default:
			return PLAYER_STATE.INVALID;
		}
	}


	public static PLAYER_STATE lookupState(int id) {
		for (PLAYER_STATE p : PLAYER_STATE.values()) {
			if (p.getState() == id) {
				return p;
			}
		}
		return PLAYER_STATE.INVALID;
	}

	public boolean canJump() {
		// TODO Auto-generated method stub
		return true;
	}

	public PlayerHandler getPlayerHandler() {
		return playerStateHandler;
		// TODO Auto-generated method stub
	}

	public ContactListener getContactListener() {
		// TODO Auto-generated method stub
		return playerContactListener;
	}
	
	public PlayerController getPlayerController() {
		return playerController;
	}

}
