package com.me.mpplatrofmer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class AnimatedB2DSprite {

	protected Body body;
	protected Animation animation;

	protected float width;
	protected float height;
	
	protected Vector2 size;
	
	public AnimatedB2DSprite(Body body, Vector2 size) {
		this.body = body;
		this.size = size;
	}
	
	public void update(float dt) {
		
		if(animation != null)
		{
			animation.update(dt);
		}
	}
	
	public void render(SpriteBatch sb) {
		
		if(animation != null)
		{
			sb.begin();
			sb.draw(animation.getFrame(), 
					(CoorMan.boxToWorld(body.getPosition().x) - size.x/2), 
					(CoorMan.boxToWorld(body.getPosition().y) - size.y/2), 
					(size.x/2), 
					(size.y/2),
					size.x,
					size.y,
					1f,
					1f,
					(float)(body.getAngle()/Math.PI*180));
			sb.end();
		}
	}
	
	public void render(SpriteBatch sb, TextureRegion f)
	{
		sb.begin();
		sb.draw(f, 
				(CoorMan.boxToWorld(body.getPosition().x) - size.x/2), 
				(CoorMan.boxToWorld(body.getPosition().y )- size.y/2), 
				(size.x/2), 
				(size.x/2),
				size.x,
				size.y,
				1f,
				1f,
				(float)(body.getAngle()/Math.PI*180));
		sb.end();
	}
	
	public Animation getAnimation() {
		return animation;
	}

	public void setAnimation(Animation animation) {
		this.animation = animation;
	}
	
	public Body getBody() { return body; }
	public Vector2 getPosition() { return body.getPosition(); }
	public float getWidth() { return width; }
	public float getHeight() { return height; }
	
}
	