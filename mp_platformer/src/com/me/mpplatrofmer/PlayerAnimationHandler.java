package com.me.mpplatrofmer;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PlayerAnimationHandler implements PlayerListener{

	private PlayerHandler playerHandler;
	private Player player;
	
	private ArrayList<Animation> animations;
	//   animation tree?
	//------------------
	//0: walking left
	//1: walking right
	//2: jumping?
	//3: dying?
	//4: buffed?
	
	public PlayerAnimationHandler(Player player)
	{
		//super();
		
		this.player = player;

		animations = new ArrayList<Animation>();
		
		player.getPlayerHandler().addListener(this);
		
	}
	
	public void addAnimation(TextureRegion[] frames, float delay)
	{
		Animation a = new Animation(frames, delay);
		animations.add(a);	
	}
	
	public void removeAnimation(Animation a)
	{
		animations.remove(a);
	}

	@Override
	public void isMovingLeft(float dt) {
		if( player.animation.isPaused() )
		{
			player.animation.setPause(false);	
		}
		//player.animation.reset();
		if(player.animation != animations.get(0))
		{
		    player.animation = animations.get(0);	
		}
		
	}

	@Override
	public void isMovingRight(float dt) {
		if( player.animation.isPaused() )
		{
			player.animation.setPause(false);	
		}
		//player.animation.reset();
		if(player.animation != animations.get(1))
		{
		    player.animation = animations.get(1);	
		}
	}

	@Override
	public void isNotMoving(float dt) {
		player.animation.setPause(true);
	}

	public void setInitialState() {
		try{
			player.animation = animations.get(0);
		}catch(NullPointerException e)
		{
			System.out.println("no initial player animation");
			System.exit(1); //terminate
		}
	}
	

	
	

}
