package com.me.mpplatrofmer;

import java.util.Vector;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.me.mpplatrofmer.gamestates.PlayState;


public class Level {
	
	
	private World world;
	private PlayState state;
	private PlatformWithFriends game;
	
	
	private Player player;
	private RayHandler rh;
	
	private Body ball1;
	private AnimatedB2DSprite ball1Sprite;
	private TextureRegion tf;
	
	public Level( PlayState state, PlatformWithFriends game)
	{
		this.state = state;
		this.world = state.getWorld();
		this.rh = state.getRh();
		this.game = game;
		this.player = state.getPlayer();
		
		makeGround();
		
		makeLights();
		
		makeBounds();
		
		makeObstacles();
		
	}

	private void makeLights() {
		
		rh.setCombinedMatrix(state.getB2dCam().combined.translate(-state.getB2dCam().viewportWidth/2, -state.getB2dCam().viewportHeight/2, 0));
		
		//new PointLight( rh, 55, Color.CYAN, 150f, 80f, 45f );
		//new PointLight( rh, 1000, Color.MAGENTA, CoorMan.worldToBox(50f), CoorMan.worldToBox(34f), CoorMan.worldToBox(45f));
		//new PointLight( rh, 1000, Color.MAGENTA, CoorMan.worldToBox(50f), CoorMan.worldToBox(34f), CoorMan.worldToBox(45f));
		
		//DirectionalLight (RayHandler rayHandler, int rays, Color color, float directionDegree)



		//new PointLight( rh, 1000, Color.CYAN, CoorMan.worldToBox(150f), CoorMan.worldToBox(110f), CoorMan.worldToBox(45f) );
		//ConeLight c1 = new ConeLight( rh, 1000, Color.BLUE, CoorMan.worldToBox(700f), CoorMan.worldToBox(80f),CoorMan.worldToBox(90f), 270, 180 );
		//ConeLight c3 = new ConeLight( rh, 1000, Color.BLUE, CoorMan.worldToBox(300f), CoorMan.worldToBox(80f),CoorMan.worldToBox(25f), 270, 20 );
		//c1.setXray(true);

	}

	private void makeObstacles() {
		
		makeBall();
		
		//makeCircle(CoorMan.worldToBox(30f), CoorMan.worldToBox(40f), CoorMan.worldToBox(5f) );
		//makeCircle(CoorMan.worldToBox(31f), CoorMan.worldToBox(10f), CoorMan.worldToBox(3f) );
		
		//makeSquare(CoorMan.worldToBox(31f), CoorMan.worldToBox(10f), CoorMan.worldToBox(3f), CoorMan.worldToBox(3f) );
		
	}

	private void makeSquare(float x, float y,
			float w, float l) {
		
		BodyDef bodyDef2 = new BodyDef();
		bodyDef2.type = BodyType.DynamicBody;
		bodyDef2.position.set(x, y);
		Body body2 = world.createBody(bodyDef2);
		
		PolygonShape p = new PolygonShape();
		p.setAsBox(w, l);

		FixtureDef fixtureDef2= new FixtureDef();
		fixtureDef2.shape = p;
		fixtureDef2.density = 20f; 
		fixtureDef2.friction = 0.4f;
		fixtureDef2.restitution = .5f; 
		Fixture fixture2 = body2.createFixture(fixtureDef2);
		p.dispose();
		
		
	}
	
	private void makeBall() {
		
		float x = CoorMan.worldToBox(32f);
		float y = CoorMan.worldToBox(40f);
		float r = CoorMan.worldToBox(5f);
		
		BodyDef bodyDef2 = new BodyDef();
		bodyDef2.type = BodyType.DynamicBody;
		bodyDef2.position.set(x, y);
		ball1 = world.createBody(bodyDef2);
		CircleShape circle2 = new CircleShape();
		circle2.setRadius(r);

		FixtureDef fixtureDef2= new FixtureDef();
		fixtureDef2.shape = circle2;
		fixtureDef2.density = 20f; 
		fixtureDef2.friction = 0.4f;
		fixtureDef2.restitution = .8f; 
		Fixture fixture2 = ball1.createFixture(fixtureDef2);
		ball1Sprite =  new AnimatedB2DSprite(ball1, new Vector2(10f, 10f));
		circle2.dispose();
		Texture t = new Texture(Gdx.files.internal("sprites/soccerball.png"));
	    tf = new TextureRegion(t);
		
	}

	private void makeCircle(float posX, float posY, float radius) 
	{

		BodyDef bodyDef2 = new BodyDef();
		bodyDef2.type = BodyType.DynamicBody;
		bodyDef2.position.set(posX, posY);
		Body body2 = world.createBody(bodyDef2);
		CircleShape circle2 = new CircleShape();
		circle2.setRadius(radius);

		FixtureDef fixtureDef2= new FixtureDef();
		fixtureDef2.shape = circle2;
		fixtureDef2.density = 20f; 
		fixtureDef2.friction = 0.4f;
		fixtureDef2.restitution = .8f; 
		Fixture fixture2 = body2.createFixture(fixtureDef2);
		circle2.dispose();
		
	}

	private void makeGround() {
		BodyDef groundBodyDef = new BodyDef();  
		
		groundBodyDef.position.set(CoorMan.worldToBox(game.getCamera().viewportWidth/2), CoorMan.worldToBox(.1f));  
		
		Body groundBody = world.createBody(groundBodyDef); 
		groundBody.setUserData("floor");
		
		PolygonShape groundBox = new PolygonShape();  
		
		groundBox.setAsBox(CoorMan.worldToBox(game.getCamera().viewportWidth), CoorMan.worldToBox(2f));
		groundBody.createFixture(groundBox, 0.0f); 
		groundBox.dispose();
		
	}
	
	public void render()
	{
		
		ball1Sprite.render(game.getSpriteBatch(), tf);
	}

	private void makeBounds() {
		//just left and right for now
		BodyDef leftBounds  = new BodyDef();  
		BodyDef rightBounds = new BodyDef();  
		BodyDef topBounds   = new BodyDef();  
		
		leftBounds.position.set(CoorMan.worldToBox(.1f), 0f);
		rightBounds.position.set(CoorMan.worldToBox(game.getCamera().viewportWidth - .1f), 0f);
		topBounds.position.set(CoorMan.worldToBox(game.getCamera().viewportWidth/2), CoorMan.worldToBox(game.getCamera().viewportHeight));
		
		Body leftBoundBody  = world.createBody(leftBounds );
		Body rightBoundBody = world.createBody(rightBounds);
		Body topBoundBody = world.createBody(topBounds);

		
		PolygonShape blocker  = new PolygonShape();    
		blocker.setAsBox(.01f, CoorMan.worldToBox(game.getCamera().viewportHeight));

		leftBoundBody.createFixture(blocker, 0f);
		rightBoundBody.createFixture(blocker, 0f);
		
		blocker.setAsBox(CoorMan.worldToBox(game.getCamera().viewportWidth), .01f);
		topBoundBody.createFixture(blocker, 0f);
		
		blocker.dispose();

	}
	
	
	
	

}
